// Javascript Synchronous vs Asynchronous

// Synchronous - statements are executed one at a time

console.log('Hello World');
// conselo.log('Hello Again');
console.log('Hello');

// Asynchronous - statements can proceed to execute while other codes are running in the background

// Fetch API - allows to asynchronously request for a resource(data)
// Get all post

console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

// Check the status of the request
/*
	Syntax:
		fetch('URL')
		.then((response)=>{
	
		})
*/
fetch('https://jsonplaceholder.typicode.com/posts')
.then(response => console.log(response.status));

// Retrieve the contents/data from the "Response" object

fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json())
.then((json) =>  console.log(json));

// Create a function using "async" and "await" keywords.
// "async" and "await" can be used to achieve asynchronous code

async function fetchData(){
	let result = await fetch('https://jsonplaceholder.typicode.com/posts');
	console.log(result);

	console.log(typeof result);

	let json = await result.json();

	console.log(json);
}
fetchData();

// Getting a specific post
// (retrieve, /post/:id, GET)
fetch('https://jsonplaceholder.typicode.com/posts/48')
.then((response) => response.json())
.then((json) => console.log(json));

// Creating a post
/*
	Syntax:
		fetch('URL', options)
		.then((response)=>{})
*/

fetch('https://jsonplaceholder.typicode.com/posts',{
	// Default method is 'GET'

	// Sets the method of the 'request' object to 'POST'
	method: 'POST',
	// Sets the header data of the "Request" object to be sent to the backend
	header: {
		'Content-type': 'application/json'
	},
	// Sets the content/body data of the "Request" object to be sent to the backend
	body: JSON.stringify({
		title: 'New post',
		body: 'Hello World!',
		userID: 1
	}),

})
// Set response pabalik kay client and convert into JSON
.then((response)=> response.json())
.then((json) => console.log(json));

// Update a post using the PUT method
fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method: 'PUT',
	headers: {
		'Content-type':'application/json'
	},
	body: JSON.stringify({
		id:1,
		title: 'Updated post',
		body: 'Hello again!',
		userID: 1
	})
})
.then((response)=> response.json())
.then((json) => console.log(json));

// Update a post using the PATCH method (update whole object)
fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method: 'PATCH',
	headers: {
		'Content-type': 'application/json',
	},
	body: JSON.stringify({
		title: 'Corrected Post'
	}),
})
.then((response)=> response.json())
.then((json)=> console.log(json));

// Delete a post (specific)
fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method: 'DELETE'
});

// Filter the posts
fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
.then((response)=> response.json())
.then((json)=> console.log(json));



